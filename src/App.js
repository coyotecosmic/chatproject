import React, {Component} from 'react';
import{CardPanel, Card, Row, Col, Input, Button} from 'react-materialize';
import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.min.js';
import firebase from 'firebase/app';
import 'firebase/database';
import Post from './Post/component/Post';
import './App.css';

class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      posts: [],
      newPostBody: '',
      loggedUser:'',
      loggedIn:false
    }
    const config = {
    apiKey: "AIzaSyDzhpiXiQIhtEAFN9fhVH5NwiLB66fpUYI",
    authDomain: "chat-96aef.firebaseapp.com",
    databaseURL: "https://chat-96aef.firebaseio.com",
    projectId: "chat-96aef",
    storageBucket: "",
    messagingSenderId: "124660395501"
  };

  // referencia al objeto de firebase
    this.app = firebase.initializeApp(config);
  // referencia a la base de datos
    this.database = this.app.database();
  //referencia a la "tabla" post
    this.databaseRef = this.database.ref().child('post');
  }

componentWillMount(){
    this.databaseRef.on('child_added', snapshot => {
        const response = snapshot.val();
        this.updateLocalState(response);
    });
}

updateLocalState = (response) =>{
  //copia del estado actual
  const posts = this.state.posts;
  //actualiza la copia del estado
  posts.push(response);
  //actualiza el estado
  posts.reverse();
  this.setState(posts);
}


//esta función concatena los valores y envía el array.
addPost = (newPostBody, loggedUser) => {
// Copiar el estado
  const postToSave = {name: this.state.loggedUser, message: this.state.newPostBody};
  //guarda en firebase nuestrops posts
  this.databaseRef.push().set(postToSave);
  this.setState({newPostBody:''})
}
//Esta función es la que gestiona el valor del input mensaje
handlePostEditorInputChange = (e) => {
  this.setState({
    newPostBody: e.target.value});
}
//Esta función es la que gestiona el valor del input usuario
handleUserNameInputChange = (e) => {
  this.setState({
    loggedUser: e.target.value});
}

//funciones loging
handleLogin = () =>{
  this.setState({ loggedIn:true});
}

handleLogout = () =>{
  this.setState({ loggedIn:false, loggedUser: '', newPostBody:''});
}


  render(){
    return(
      <Row className="mainContainter">
      <h4>Hello Chat</h4>
    <Col s={12}>
        <CardPanel className="lighten-4 black-text messagebox">
        {this.state.posts.map((item, id) => {
          return (<Post key={id} userName={item.name} postBody={item.message}/>)
        })}
        </CardPanel>

        <CardPanel className="lighten-4 black-text loginBox" data-isVisible={this.state.loggedIn}>
        <h5>Haz login para comenzar el chat</h5>
        <Input onChange={this.handleUserNameInputChange} placeholder="ur name buddy" s={12} label="Name" value={this.state.loggedUser} />
        <Button onClick={this.handleLogin} waves='light' node='a' large={true}> Entrar </Button>
        </CardPanel>

        <CardPanel className="lighten-4 black-text messageInput" data-isVisible={this.state.loggedIn}>
        <h5>Hola {this.state.loggedUser}!!</h5>
        <Input onChange={this.handlePostEditorInputChange} placeholder="Talk to me" s={12} label="Message" value={this.state.newPostBody} />
        <Button onClick={this.handleLogout} waves='light' node='a' large={true}> Logout </Button>
        <Button className="sendBtn" onClick={this.addPost} waves='light' node='a'> Send your message </Button>
        </CardPanel>
    </Col>
      </Row>
    )
  }
}

export default App;
